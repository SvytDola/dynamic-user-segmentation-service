package app

import (
	"dynamic-user-segmentation-service/controller"
	"dynamic-user-segmentation-service/mapper"
	"dynamic-user-segmentation-service/model"
	"dynamic-user-segmentation-service/service"
	"github.com/gin-gonic/gin"
	"github.com/jellydator/ttlcache/v3"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/gorm"
	"time"
)

func InitApp(engine *gin.Engine, db *gorm.DB, swagger bool) {
	ttlCache := ttlcache.New[string, model.TtlModel](
		ttlcache.WithTTL[string, model.TtlModel](10 * time.Second),
	)

	segmentMapper := &mapper.SegmentMapper{}
	segmentService := &service.SegmentServiceImpl{Database: db, SegmentMapper: segmentMapper}
	segmentController := &controller.SegmentControllerImpl{SegmentService: segmentService}

	csvFileService := &service.CsvFileServiceImpl{Database: db}
	transactionService := &service.TransactionServiceImpl{Database: db, CsvFileService: csvFileService}

	userMapper := &mapper.UserMapper{}
	userService := &service.UserServiceImpl{
		Database:           db,
		UserMapper:         userMapper,
		TransactionService: transactionService,
		SegmentService:     segmentService,
		TttCache:           ttlCache,
	}
	ttlCache.OnEviction(userService.OnDeleteFromTtlCache)
	go ttlCache.Start()
	userController := &controller.UserControllerImpl{
		UserService:        userService,
		TransactionService: transactionService}

	transactionController := &controller.TransactionControllerImpl{
		TransactionService: transactionService,
		PathToGetCsvFile:   "/api/v1/transaction/csv",
		CsvFileService:     csvFileService,
	}
	v1 := engine.Group("/api/v1")
	{
		segments := v1.Group("/segment")
		{
			segments.POST("", segmentController.CreateSegment)
			segments.DELETE("", segmentController.DeleteSegment)
		}
		users := v1.Group("/user")
		{
			users.POST("", userController.CreateUser)
			users.POST("/segments", userController.UpdateUserSegments)
			users.POST("/segment", userController.AddUserInSegment)
			users.GET("/:id", userController.GetUserById)
		}
		transactions := v1.Group("/transaction")
		{
			transactions.POST("/csv", transactionController.CreateCsvFileWithTransactions)
		}
	}

	engine.GET(transactionController.PathToGetCsvFile+"/:id", transactionController.GetCsvFileWithTransactions)

	if swagger == true {
		engine.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

}
