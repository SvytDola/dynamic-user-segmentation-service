package model

import "time"

// CsvFileCreateRequest
// @Description Запрос на создание csv файла с историей транзакций пользователя.
type CsvFileCreateRequest struct {

	// Идентификатор пользователя
	UserId int64 `json:"userId" binding:"required"`

	// С какого времени начинать поиск, формат времени (RFC 3339)
	Start time.Time `json:"start" example:"2020-08-27T14:49:29+00:00"`

	// По какое время искать, формат времени (RFC 3339)
	End time.Time `json:"end" example:"2024-08-27T14:49:29+00:00"`
}
