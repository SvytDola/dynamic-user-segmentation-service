package model

import "time"

// ApiError
// @Description DTO с информацией о пользователе
type ApiError struct {

	// Сообщение ошибки
	Message string `json:"message" binding:"required"`

	// Время ошибки
	Timestamp time.Time `json:"timestamp" binding:"required"`

	// Статус ошибки
	StatusCode int `json:"-"`
}
