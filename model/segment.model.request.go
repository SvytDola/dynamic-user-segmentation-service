package model

// SegmentCreateRequest
// @Description Запрос на создание сегмента
type SegmentCreateRequest struct {

	// Название сегмента.
	Slug string `json:"slug" example:"AVITO_DISCOUNT_50" binding:"required"`
}
