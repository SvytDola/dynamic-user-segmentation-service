package model

// UserResponse
// @Description DTO с информацией о пользователе
type UserResponse struct {

	// Идентификатор пользователя
	Id int64 `json:"id" binding:"required"`
}

// UserInfoResponse
// @Description DTO с информацией о пользователе и его сегментах
type UserInfoResponse struct {

	// Идентификатор пользователя
	Id int64 `json:"id" binding:"required"`

	// Сегменты, в которых состоит пользователь
	Segments []SegmentResponse `json:"segments" binding:"required"`
}
