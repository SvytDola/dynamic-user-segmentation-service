package model

// SegmentResponse
// @Description DTO с информацией о пользователе
type SegmentResponse struct {

	// Идентификатор сегмента
	Id int64 `json:"id" binding:"required"`

	// Название сегмента
	Slug string `json:"slug" example:"AVITO_PERFORMANCE_VAS" binding:"required"`
}
