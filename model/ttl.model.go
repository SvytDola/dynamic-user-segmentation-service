package model

type TtlModel struct {

	// Название сегмента
	Slug string

	// Идентификатор сегмента
	UserId int64
}
