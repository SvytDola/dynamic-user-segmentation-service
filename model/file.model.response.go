package model

// CsvFileResponse
// @Description DTO с ссылкой на csv с транзацкциями.
type CsvFileResponse struct {

	// Ссылка на csv файл с транзацкциями
	Link string `json:"link"`
}
