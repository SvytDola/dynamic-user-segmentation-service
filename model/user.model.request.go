package model

import "time"

// UpdateUserSegmentsRequest
// @Description Запрос на обновление сегментов пользователя
type UpdateUserSegmentsRequest struct {

	// Идентификатор пользователя
	UserId int64 `json:"userId" binding:"required"`

	// Названия сегментов, в которые мы хотим добавить пользователя
	ShouldBeAddedSlugs []string `json:"shouldBeAddedSlugs"  example:"AVITO_PERFORMANCE_VAS" binding:"required"`

	// Названия сегментов, из которых мы хотим исключить пользователя
	ShouldBeRemovedSlugs []string `json:"shouldBeRemovedSlugs" example:"AVITO_DISCOUNT_30" binding:"required"`
}

// AddUserInSegmentRequest
// @Description Запрос на добавление пользователя в сегмент на время
type AddUserInSegmentRequest struct {

	// Идентификатор пользователя
	UserId int64 `json:"userId" binding:"required"`

	// Название сегмента
	Slug string `json:"slug" binding:"required" example:"AVITO_DISCOUNT_50"`

	// Дата удаления
	ExpirationDate time.Time `json:"expirationDate" binding:"required" example:"2024-08-27T14:49:29+00:00"`
}
