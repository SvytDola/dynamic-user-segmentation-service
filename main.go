package main

import (
	segmentsApp "dynamic-user-segmentation-service/app"
	"dynamic-user-segmentation-service/database"
	"dynamic-user-segmentation-service/docs"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/ilyakaznacheev/cleanenv"
	"gorm.io/driver/postgres"
	"log"
)

// @contact.name   Shuvi
// @contact.url    https://t.me/undoh
// @contact.email  undoh@yandex.ru

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html
func main() {

	var configDatabase database.ConfigDatabase

	err := cleanenv.ReadEnv(&configDatabase)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(configDatabase)
	swaggerEnable := false

	if configDatabase.Swagger == "true" {
		swaggerEnable = true
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		configDatabase.Host, configDatabase.User, configDatabase.Password, configDatabase.Name, configDatabase.Port)

	db := database.ConnectToDatabase(postgres.Open(dsn))

	database.AutoMigrate(db)

	docs.SwaggerInfo.Title = "Сервис динамического сегментирования пользователей"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Schemes = []string{"http"}

	r := gin.Default()
	segmentsApp.InitApp(r, db, swaggerEnable)
	r.Run(":8080")
}
