package main_test

import (
	"bytes"
	"dynamic-user-segmentation-service/app"
	"dynamic-user-segmentation-service/database"
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"slices"
	"strconv"
	"testing"
	"time"
)

var engine *gin.Engine

func TestMain(m *testing.M) {

	var configDatabase database.TestConfigDatabase

	err := cleanenv.ReadEnv(&configDatabase)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(configDatabase)
	swaggerEnable := false

	if configDatabase.Swagger == "true" {
		swaggerEnable = true
	}

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		configDatabase.Host, configDatabase.User, configDatabase.Password, configDatabase.Name, configDatabase.Port)
	db := database.ConnectToDatabase(postgres.Open(dsn))

	database.AutoMigrate(db)

	engine = gin.Default()
	app.InitApp(engine, db, swaggerEnable)

	code := m.Run()

	err = db.Migrator().DropTable(
		&entity.UserEntity{},
		&entity.SegmentEntity{},
		&entity.TransactionEntity{},
		"user_segments",
		&entity.CsvFileEntity{},
	)

	if err != nil {
		log.Printf("Failed to drop test tables: %v", err)
	}

	os.Exit(code)

}

func TestCrud(t *testing.T) {
	avitoPerfomanceVas := "AVITO_PERFORMANCE_VAS"
	avitoVoiceMessages := "AVITO_VOICE_MESSAGES"
	avitoDiscount30 := "AVITO_DISCOUNT_30"
	avitoDiscount50 := "AVITO_DISCOUNT_50"

	slugs := []string{avitoPerfomanceVas, avitoVoiceMessages, avitoDiscount30, avitoDiscount50}

	segmentCreated := createSegmentTestCase(t, avitoVoiceMessages)
	deleteSegmentBySlagTestCase(t, segmentCreated.Slug)

	for _, slug := range slugs {
		createSegmentTestCase(t, slug)
	}

	userResponse := createUserTestCase(t)

	shouldBeAdded := []string{avitoVoiceMessages, avitoDiscount30, avitoPerfomanceVas}
	shouldBeRemoved := []string{avitoVoiceMessages, avitoDiscount30}
	updateUserToSegmentsTestCase(t, userResponse.Id, shouldBeAdded, shouldBeRemoved)

	getUserInfoByIdTestCase(t, userResponse.Id)

	startTime, _ := time.Parse("YY", "20")
	endTime := time.Now()
	getUserHistoryTestCase(t, userResponse.Id, startTime, endTime)

	expirationDate := time.Now().Add(time.Second * 10)
	addUserInSegmentTestCase(t, userResponse.Id, expirationDate, avitoVoiceMessages)
}

func addUserInSegmentTestCase(t *testing.T, id int64, expirationDate time.Time, slug string) {
	urlTemplate := "/api/v1/user/segment"

	segmentCreateRequest := model.AddUserInSegmentRequest{UserId: id, Slug: slug, ExpirationDate: expirationDate}
	jsonValue, _ := json.Marshal(segmentCreateRequest)

	req, _ := http.NewRequest("POST", urlTemplate, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var userResponse model.UserInfoResponse

	err := json.Unmarshal(w.Body.Bytes(), &userResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.NotEqual(t, 0, userResponse.Id)
}

func getUserHistoryTestCase(t *testing.T, id int64, startTime time.Time, endTime time.Time) {
	urlTemplate := "/api/v1/transaction/csv"

	segmentCreateRequest := model.CsvFileCreateRequest{UserId: id, Start: startTime, End: endTime}
	jsonValue, _ := json.Marshal(segmentCreateRequest)

	req, _ := http.NewRequest("POST", urlTemplate, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)
	log.Println("Response ", w.Body.String())

	var transactionResponse model.CsvFileResponse

	err := json.Unmarshal(w.Body.Bytes(), &transactionResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.NotEqual(t, "", transactionResponse.Link)
}

func createUserTestCase(t *testing.T) model.UserResponse {
	urlTemplate := "/api/v1/user"

	req, _ := http.NewRequest("POST", urlTemplate, nil)

	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)
	log.Println("Response ", w.Body.String())

	var userResponse model.UserResponse

	err := json.Unmarshal(w.Body.Bytes(), &userResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.NotEqual(t, 0, userResponse.Id)

	return userResponse
}

func createSegmentTestCase(t *testing.T, slug string) model.SegmentResponse {
	urlTemplate := "/api/v1/segment"

	segmentCreateRequest := model.SegmentCreateRequest{Slug: slug}

	jsonValue, _ := json.Marshal(segmentCreateRequest)
	req, _ := http.NewRequest("POST", urlTemplate, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)
	log.Println("Response ", w.Body.String())

	var segmentCreated model.SegmentResponse

	err := json.Unmarshal(w.Body.Bytes(), &segmentCreated)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, slug, segmentCreated.Slug)
	assert.NotEqual(t, 0, segmentCreated.Id)

	return segmentCreated
}

func deleteSegmentBySlagTestCase(t *testing.T, slug string) {
	urlTemplate := "/api/v1/segment?slug=" + slug

	req, _ := http.NewRequest("DELETE", urlTemplate, nil)
	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNoContent, w.Code)
	log.Println("Response ", w.Body.String())
}

func updateUserToSegmentsTestCase(t *testing.T, id int64, shouldBeAddedSlugs []string, shouldBeRemovedSlugs []string) {
	urlTemplate := "/api/v1/user/segments"

	segmentAddUserRequest := model.UpdateUserSegmentsRequest{
		UserId:               id,
		ShouldBeAddedSlugs:   shouldBeAddedSlugs,
		ShouldBeRemovedSlugs: shouldBeRemovedSlugs,
	}
	jsonValue, _ := json.Marshal(segmentAddUserRequest)
	req, _ := http.NewRequest("POST", urlTemplate, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var userInfoResponse model.UserInfoResponse

	err := json.Unmarshal(w.Body.Bytes(), &userInfoResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, id, userInfoResponse.Id)

	var slugsCurrent []string
	for _, segment := range userInfoResponse.Segments {
		slugsCurrent = append(slugsCurrent, segment.Slug)
	}

	var slugExpected []string
	for _, slug := range shouldBeAddedSlugs {
		contains := slices.Contains(shouldBeRemovedSlugs, slug)
		if contains == false {
			slugExpected = append(slugExpected, slug)
		}
	}

	assert.ElementsMatch(t, slugExpected, slugsCurrent)
}

func getUserInfoByIdTestCase(t *testing.T, id int64) {
	urlTemplate := "/api/v1/user/" + strconv.FormatInt(id, 10)

	req, _ := http.NewRequest("GET", urlTemplate, nil)
	w := httptest.NewRecorder()
	engine.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var userInfoResponse model.UserInfoResponse

	err := json.Unmarshal(w.Body.Bytes(), &userInfoResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, id, userInfoResponse.Id)
}
