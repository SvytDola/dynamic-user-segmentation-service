package service

import (
	"context"
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
	"github.com/jellydator/ttlcache/v3"
)

// UserService Описание сервиса для работы с сущеностью entity.UserEntity
type UserService interface {

	// CreateUser Создание пользователя.
	CreateUser() (*model.UserResponse, *model.ApiError)

	// GetEntityById Получить информацию о пользователе по идентификатору.
	GetEntityById(id int64) (*entity.UserEntity, *model.ApiError)

	// UpdateUserSegments Обновление информации о текущих сегментах пользователя.
	UpdateUserSegments(request *model.UpdateUserSegmentsRequest) (*model.UserInfoResponse, *model.ApiError)

	// FindUserById Получить DTO с информации о пользователе по идентификатору.
	FindUserById(id int64) (*model.UserInfoResponse, *model.ApiError)

	// AddUserInSegmentWithExpirationDate Добавить пользователя в сегмент на время
	AddUserInSegmentWithExpirationDate(request model.AddUserInSegmentRequest) (*model.UserInfoResponse, *model.ApiError)

	OnDeleteFromTtlCache(ctx context.Context, reason ttlcache.EvictionReason, item *ttlcache.Item[string, model.TtlModel])
}
