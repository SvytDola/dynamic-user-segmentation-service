package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
	"dynamic-user-segmentation-service/util"
	"fmt"
	"gorm.io/gorm"
	"net/http"
)

// CsvFileServiceImpl Реализация сервиса создания csv файлов
type CsvFileServiceImpl struct {
	CsvFileService

	Database *gorm.DB
}

const FileNotFoundByIdErrorMessage = "File not found by id %d."

func (s *CsvFileServiceImpl) CreateFileWithTransactions(transactions []entity.TransactionEntity) (*entity.CsvFileEntity, *model.ApiError) {
	out := util.MarshalString(transactions)

	fileEntity := entity.CsvFileEntity{Text: out}
	err := s.Database.Create(&fileEntity).Error
	if err != nil {
		return nil, &model.ApiError{Message: err.Error(), StatusCode: http.StatusInternalServerError}
	}

	return &fileEntity, nil
}
func (s *CsvFileServiceImpl) GetById(id int64) (*entity.CsvFileEntity, *model.ApiError) {
	var file entity.CsvFileEntity
	if err := s.Database.Where("id = ?", id).First(&file).Error; err != nil {
		return nil, &model.ApiError{
			Message:    fmt.Sprintf(FileNotFoundByIdErrorMessage, id),
			StatusCode: http.StatusNotFound,
		}
	}

	return &file, nil
}
