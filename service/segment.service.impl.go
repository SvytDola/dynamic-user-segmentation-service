package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/mapper"
	"dynamic-user-segmentation-service/model"
	"fmt"
	"gorm.io/gorm"
	"net/http"
)

// SegmentServiceImpl Реализация сервиса для работы с сущностью entity.SegmentEntity
type SegmentServiceImpl struct {
	SegmentService
	Database      *gorm.DB
	SegmentMapper *mapper.SegmentMapper
}

const SegmentNotFoundByNameErrorMessage = "Segment not found by name %s."

func (s *SegmentServiceImpl) CreateSegment(request *model.SegmentCreateRequest) (*model.SegmentResponse, *model.ApiError) {
	segmentEntity := s.SegmentMapper.ToEntity(request)

	err := s.Database.Create(segmentEntity).Error
	if err != nil {
		return nil, &model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest}
	}

	return s.SegmentMapper.ToResponse(segmentEntity), nil
}

func (s *SegmentServiceImpl) DeleteSegmentBySlug(slug string) *model.ApiError {
	var segmentEntity entity.SegmentEntity
	err := s.Database.Where("slug = ?", slug).First(&segmentEntity).Error
	if err != nil {
		return &model.ApiError{
			Message:    fmt.Sprintf(SegmentNotFoundByNameErrorMessage, slug),
			StatusCode: http.StatusNotFound,
		}
	}
	s.Database.Delete(segmentEntity)
	return nil
}

func (s *SegmentServiceImpl) FindBySlugs(slugs []string) ([]entity.SegmentEntity, *model.ApiError) {
	var segmentsRequests []entity.SegmentEntity
	dbErr := s.Database.
		Where("slug in ?", slugs).
		Find(&segmentsRequests).Error
	if dbErr != nil {
		return nil, &model.ApiError{Message: dbErr.Error(), StatusCode: http.StatusBadRequest}
	}
	return segmentsRequests, nil
}
