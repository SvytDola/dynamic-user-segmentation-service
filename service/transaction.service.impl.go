package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
	"gorm.io/gorm"
	"net/http"
)

// TransactionServiceImpl Реализация сервиса транзакций
type TransactionServiceImpl struct {
	TransactionService

	Database       *gorm.DB
	CsvFileService CsvFileService
}

func (t *TransactionServiceImpl) FindAllByUserId(id int64) ([]entity.TransactionEntity, *model.ApiError) {
	var transactions []entity.TransactionEntity
	err := t.Database.Where("user_id = ?", id).Find(&transactions).Error
	if err != nil {
		return nil, &model.ApiError{StatusCode: http.StatusInternalServerError, Message: err.Error()}
	}
	return transactions, nil
}

func (t *TransactionServiceImpl) Create(transactions []entity.TransactionEntity) *model.ApiError {
	err := t.Database.Create(&transactions).Error
	if err != nil {
		return &model.ApiError{StatusCode: http.StatusBadRequest, Message: "Existing segment"}
	}
	return nil
}

func (t *TransactionServiceImpl) CreateCsvFileWithTransactions(
	request model.CsvFileCreateRequest) (*entity.CsvFileEntity, *model.ApiError) {
	var transactions []entity.TransactionEntity
	err := t.Database.Preload("Segment").Where("created_at >= ? and created_at <= ?", request.Start, request.End).
		Find(&transactions).Error
	if err != nil {
		return nil, &model.ApiError{StatusCode: http.StatusBadRequest, Message: err.Error()}
	}

	file, apiError := t.CsvFileService.CreateFileWithTransactions(transactions)

	if apiError != nil {
		return nil, apiError
	}

	return file, nil
}
