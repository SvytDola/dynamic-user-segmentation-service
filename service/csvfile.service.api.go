package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
)

// CsvFileService Описание сервиса создания csv файлов
type CsvFileService interface {

	// CreateFileWithTransactions Создать csv файл с транзакциями
	CreateFileWithTransactions(transactions []entity.TransactionEntity) (*entity.CsvFileEntity, *model.ApiError)

	// GetById Получить содержание файла по его идентификатору
	GetById(id int64) (*entity.CsvFileEntity, *model.ApiError)
}
