package service

import (
	"context"
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/mapper"
	"dynamic-user-segmentation-service/model"
	"fmt"
	"github.com/jellydator/ttlcache/v3"
	"gorm.io/gorm"
	"log"
	"net/http"
	"slices"
	"strconv"
	"time"
)

// UserServiceImpl Реализация сервиса для работы с сущеностью entity.UserEntity
type UserServiceImpl struct {
	UserService

	TttCache           *ttlcache.Cache[string, model.TtlModel]
	Database           *gorm.DB
	UserMapper         *mapper.UserMapper
	TransactionService TransactionService
	SegmentService     SegmentService
}

const UserNotFoundByIdErrorMessage = "User not found by id - %d."

func (s *UserServiceImpl) CreateUser() (*model.UserResponse, *model.ApiError) {
	var userCreate entity.UserEntity
	err := s.Database.Create(&userCreate).Error
	if err != nil {
		return nil, &model.ApiError{Message: err.Error(), StatusCode: http.StatusInternalServerError}
	}
	return &model.UserResponse{Id: userCreate.Id}, nil
}

func (s *UserServiceImpl) GetEntityById(id int64) (*entity.UserEntity, *model.ApiError) {
	var userCreate entity.UserEntity
	err := s.Database.Preload("Segments").First(&userCreate, id).Error
	if err != nil {
		return nil, &model.ApiError{Message: fmt.Sprintf(UserNotFoundByIdErrorMessage, id)}
	}
	return &userCreate, nil
}

func (s *UserServiceImpl) UpdateUserSegments(
	updateUserSegmentsRequest *model.UpdateUserSegmentsRequest) (*model.UserInfoResponse, *model.ApiError) {

	userEntity, err := s.GetEntityById(updateUserSegmentsRequest.UserId)
	if err != nil {
		return nil, err
	}

	segmentsRequests, apiError := s.SegmentService.FindBySlugs(updateUserSegmentsRequest.ShouldBeAddedSlugs)
	if apiError != nil {
		return nil, apiError
	}

	s.addSegmentsInEntity(userEntity, segmentsRequests)
	s.removeSegmentsBySlugsFromEntity(userEntity, updateUserSegmentsRequest.ShouldBeRemovedSlugs)

	databaseError := s.Database.Model(userEntity).Association("Segments").Replace(userEntity.Segments)
	if databaseError != nil {
		return nil, &model.ApiError{Message: databaseError.Error(), StatusCode: http.StatusInternalServerError}
	}
	err = s.TransactionService.Create(userEntity.Transactions)
	if err != nil {
		return nil, err
	}

	return s.UserMapper.ToResponse(userEntity), nil
}

func (s *UserServiceImpl) FindUserById(id int64) (*model.UserInfoResponse, *model.ApiError) {
	userEntity, apiError := s.GetEntityById(id)
	if apiError != nil {
		return nil, apiError
	}
	return s.UserMapper.ToResponse(userEntity), nil
}

func (s *UserServiceImpl) addSegmentsInEntity(userEntity *entity.UserEntity, segmentsRequests []entity.SegmentEntity) {
	for _, segment := range segmentsRequests {
		if segment.Contains(userEntity.Segments) == false {
			userEntity.Segments = append(userEntity.Segments, segment)
			userEntity.Transactions = append(
				userEntity.Transactions,
				entity.TransactionEntity{UserId: userEntity.Id, Segment: segment, Type: "ADD"},
			)
		}
	}
}

func (s *UserServiceImpl) removeSegmentsBySlugsFromEntity(userEntity *entity.UserEntity, slugs []string) {
	var segments []entity.SegmentEntity
	for _, segment := range userEntity.Segments {
		contains := slices.Contains(slugs, segment.Slug)
		if contains == false {
			segments = append(segments, segment)
		} else {
			userEntity.Transactions = append(
				userEntity.Transactions,
				entity.TransactionEntity{UserId: userEntity.Id, Segment: segment, Type: "REMOVE"},
			)
		}
	}
	userEntity.Segments = segments
}

func (s *UserServiceImpl) AddUserInSegmentWithExpirationDate(
	request model.AddUserInSegmentRequest) (*model.UserInfoResponse, *model.ApiError) {

	segmentsRequest := model.UpdateUserSegmentsRequest{
		UserId:               request.UserId,
		ShouldBeRemovedSlugs: []string{},
		ShouldBeAddedSlugs:   []string{request.Slug},
	}

	userInfoResponse, apiError := s.UpdateUserSegments(&segmentsRequest)
	if apiError != nil {
		return nil, apiError
	}

	ttlModel := model.TtlModel{Slug: request.Slug, UserId: request.UserId}
	key := strconv.FormatInt(request.UserId, 10) + request.Slug
	duration := request.ExpirationDate.Sub(time.Now())
	s.TttCache.Set(key, ttlModel, duration)
	log.Printf("User with id %d has been added in segment %s.\n", ttlModel.UserId, ttlModel.Slug)
	return userInfoResponse, nil
}

func (s *UserServiceImpl) OnDeleteFromTtlCache(_ context.Context, reason ttlcache.EvictionReason, item *ttlcache.Item[string, model.TtlModel]) {
	if reason == ttlcache.EvictionReasonExpired {
		value := item.Value()
		segmentsRequest := model.UpdateUserSegmentsRequest{
			UserId:               value.UserId,
			ShouldBeRemovedSlugs: []string{value.Slug},
			ShouldBeAddedSlugs:   []string{},
		}

		_, apiError := s.UpdateUserSegments(&segmentsRequest)
		if apiError != nil {
			log.Println(apiError.Message)
			return
		}
		log.Printf("User with id %d has been deleted from %s.\n", value.UserId, value.Slug)
	}
}
