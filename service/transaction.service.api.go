package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
)

// TransactionService Описание сервиса сущности "Транзакция"
type TransactionService interface {

	// FindAllByUserId Получить все транзакции по идентификатору пользователя
	FindAllByUserId(id int64) ([]entity.TransactionEntity, *model.ApiError)

	// Create создать транзакцию.
	Create(transactions []entity.TransactionEntity) *model.ApiError

	// CreateCsvFileWithTransactions Создать файл со списком транзакций
	CreateCsvFileWithTransactions(request model.CsvFileCreateRequest) (*entity.CsvFileEntity, *model.ApiError)
}
