package service

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
)

// SegmentService Описание сервиса для работы с сущностью entity.SegmentEntity
type SegmentService interface {

	// CreateSegment Создание новоего сегмента.
	CreateSegment(request *model.SegmentCreateRequest) (*model.SegmentResponse, *model.ApiError)

	// DeleteSegmentBySlug Удаление сегмента по его имени (slug).
	DeleteSegmentBySlug(slug string) *model.ApiError

	// FindBySlugs Поиск сегментов по их именам
	FindBySlugs(slugs []string) ([]entity.SegmentEntity, *model.ApiError)
}
