package util

import (
	"dynamic-user-segmentation-service/model"
	"github.com/gin-gonic/gin"
	"time"
)

// SendError Записывает в контекст ошибку в json формате и автоматически устанавливает время.
func SendError(context *gin.Context, apiError *model.ApiError) {
	apiError.Timestamp = time.Now()
	context.JSON(apiError.StatusCode, apiError)
}
