package util

import (
	"dynamic-user-segmentation-service/entity"
	"strconv"
)

func MarshalString(transactions []entity.TransactionEntity) string {
	str := "user_id,slug,type,time\n"
	for _, transaction := range transactions {
		str += strconv.FormatInt(transaction.Id, 10) + ","
		str += transaction.Segment.Slug + ","
		str += transaction.Type + ","
		str += transaction.CreatedAt.Format("2006-01-02T15:04:05Z07:00")
	}
	return str
}
