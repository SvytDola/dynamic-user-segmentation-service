package controller

import "github.com/gin-gonic/gin"

// TransactionController Описание контроллера транзакций
type TransactionController interface {

	// CreateCsvFileWithTransactions Получение транзакций пользователя в промежутке времени
	//
	// Body: model.CsvFileCreateRequest - идентификатор пользователя
	// Returns: model.CsvFileResponse - DTO, содержащая ссылку на csvFile
	CreateCsvFileWithTransactions(ctx *gin.Context)

	// GetCsvFileWithTransactions Получение текста из csv file с транзакциями
	//
	// Param: id - идентификатор файла + .csv
	// Returns: Содержание файла
	GetCsvFileWithTransactions(ctx *gin.Context)
}
