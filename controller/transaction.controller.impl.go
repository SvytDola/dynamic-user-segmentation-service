package controller

import (
	"dynamic-user-segmentation-service/model"
	"dynamic-user-segmentation-service/service"
	"dynamic-user-segmentation-service/util"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
)

// TransactionControllerImpl Реализация контроллера транзакций
type TransactionControllerImpl struct {
	TransactionController

	PathToGetCsvFile   string
	TransactionService service.TransactionService
	CsvFileService     service.CsvFileService
}

// CreateCsvFileWithTransactions godoc
//
//		@Summary		Получить ссылку на CSV файл с историей добавления и удаления в сегменты
//		@Description	Получить ссылку на CSV файл с историей добавления и удаления в сегменты
//		@Tags			History API
//		@Produce		json
//	    @Param	        request    body  model.CsvFileCreateRequest true "Запрос на создание csv file"
//		@Success		201
//		@Failure        400 {object}    model.ApiError
//		@Router			/api/v1/transaction/csv [post]
func (c *TransactionControllerImpl) CreateCsvFileWithTransactions(ctx *gin.Context) {
	var request model.CsvFileCreateRequest
	if err := ctx.ShouldBindJSON(&request); err != nil {
		util.SendError(ctx, &model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest})
		return
	}

	csvFileEntity, apiError := c.TransactionService.CreateCsvFileWithTransactions(request)
	if apiError != nil {
		util.SendError(ctx, apiError)
		return
	}

	serverUrl := "http://" + ctx.Request.Host
	idString := strconv.FormatInt(csvFileEntity.Id, 10)
	response := model.CsvFileResponse{
		Link: serverUrl + c.PathToGetCsvFile + "/" + idString + ".csv",
	}
	ctx.JSON(http.StatusCreated, response)
}

func (c *TransactionControllerImpl) GetCsvFileWithTransactions(ctx *gin.Context) {
	param := strings.Split(ctx.Param("id"), ".")[0]
	id, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		util.SendError(ctx, &model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest})
		return
	}
	fileEntity, apiError := c.CsvFileService.GetById(id)
	if apiError != nil {
		util.SendError(ctx, apiError)
		return
	}

	ctx.Header("Content-Description", "File Transfer")
	ctx.Header("Content-Transfer-Encoding", "binary")
	ctx.Header("Content-Disposition", "attachment; filename="+strconv.FormatInt(fileEntity.Id, 10)+".csv")
	ctx.Header("Content-Type", "application/octet-stream")
	ctx.String(http.StatusOK, fileEntity.Text)
}
