package controller

import (
	"dynamic-user-segmentation-service/model"
	"dynamic-user-segmentation-service/service"
	"dynamic-user-segmentation-service/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

// SegmentControllerImpl Реализация контроллера сегментов.
type SegmentControllerImpl struct {
	SegmentController

	SegmentService service.SegmentService
}

// CreateSegment godoc
//
//		@Summary		Создание сегмента
//		@Description	Создание сегмента
//		@Tags			Segment API
//		@Accept			json
//		@Produce		json
//	    @Param	        segment body    model.SegmentCreateRequest true	"Запрос на создание сегмента"
//		@Success		201	{object}	model.SegmentResponse "DTO с информацией о пользователе"
//		@Failure        400 {object}    model.ApiError
//		@Router			/api/v1/segment [post]
func (c *SegmentControllerImpl) CreateSegment(context *gin.Context) {
	var segmentCreateRequest model.SegmentCreateRequest

	if err := context.ShouldBindJSON(&segmentCreateRequest); err != nil {
		apiError := model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest}
		util.SendError(context, &apiError)
		return
	}

	segmentCreated, err := c.SegmentService.CreateSegment(&segmentCreateRequest)
	if err != nil {
		util.SendError(context, err)
		return
	}

	context.JSON(http.StatusCreated, segmentCreated)
}

// DeleteSegment godoc
//
//		@Summary		Delete segment
//		@Description	Delete segment
//		@Tags			Segment API
//		@Produce		json
//	    @Param	        slug    query  string true	"Название сегмента"
//		@Success		204
//		@Failure        404 {object}    model.ApiError "Сегмент не найден"
//		@Failure        400 {object}    model.ApiError "Query slug not set"
//		@Router			/api/v1/segment [delete]
func (c *SegmentControllerImpl) DeleteSegment(context *gin.Context) {
	if err := context.ShouldBindQuery("slug"); err != nil {
		util.SendError(context, &model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest})
	}

	slug := context.Query("slug")

	if err := c.SegmentService.DeleteSegmentBySlug(slug); err != nil {
		util.SendError(context, err)
		return
	}
	context.JSON(http.StatusNoContent, nil)
}
