package controller

import "github.com/gin-gonic/gin"

// SegmentController Описание контроллера сегментов
type SegmentController interface {

	// CreateSegment Handler создания сегмента
	//
	// Body: model.SegmentCreateRequest
	// Returns: model.SegmentResponse
	CreateSegment(context *gin.Context)

	// DeleteSegment Handler удаления сегмента
	//
	// Query: slug - навзание сегмента
	// Returns: model.UserInfoResponse
	DeleteSegment(context *gin.Context)
}
