package controller

import "github.com/gin-gonic/gin"

// UserController Описание контроллера пользоватлей
type UserController interface {

	// CreateUser Handler создания пользователя
	//
	// Returns: model.UserResponse
	CreateUser(context *gin.Context)

	// UpdateUserSegments Handler обновления сегментов пользователя
	//
	// Body: model.UpdateUserSegmentsRequest
	// Returns: model.UserInfoResponse
	UpdateUserSegments(context *gin.Context)

	// GetUserById Handler получения информации о пользователе и его сегментах
	//
	// Param: id
	// Returns: model.UserInfoResponse
	GetUserById(context *gin.Context)

	// AddUserInSegment Handler добавления пользователя в сегмент на время
	//
	// Body: model.AddUserInSegmentRequest
	// Returns: model.UserInfoResponse
	AddUserInSegment(context *gin.Context)
}
