package controller

import (
	"dynamic-user-segmentation-service/model"
	"dynamic-user-segmentation-service/service"
	"dynamic-user-segmentation-service/util"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// UserControllerImpl Реализация контроллера пользователей.
type UserControllerImpl struct {
	UserController

	TransactionService service.TransactionService
	UserService        service.UserService
}

// CreateUser godoc
//
//	@Summary		Метод создания пользователя
//	@Description	Метод создания пользователя
//	@Tags			User API
//	@Produce		json
//	@Success		201	{object}	model.UserResponse "DTO с информацией о пользователе"
//	@Failure        400 {object}    model.ApiError "Неправильный запрос"
//	@Router			/api/v1/user [post]
func (c *UserControllerImpl) CreateUser(context *gin.Context) {
	userCreated, err := c.UserService.CreateUser()
	if err != nil {
		util.SendError(context, err)
		return
	}

	context.JSON(http.StatusCreated, userCreated)
}

// UpdateUserSegments godoc
//
//		@Summary		Метод добавления или ужаления пользователя в сегменты
//		@Description	Метод добавления или ужаления пользователя в сегменты
//		@Tags			User API
//		@Accept			json
//		@Produce		json
//	 	@Param 			example 		body model.UpdateUserSegmentsRequest true "Запрос на обновление информации о сегментах пользователя"
//		@Success		200	{object}	model.UserInfoResponse "DTO с информацией о пользователе и его сегментах"
//		@Failure        404 {object}    model.ApiError "User not found"
//		@Router			/api/v1/user/segments [post]
func (c *UserControllerImpl) UpdateUserSegments(context *gin.Context) {
	var addUserInSegmentsRequest model.UpdateUserSegmentsRequest

	if err := context.ShouldBindJSON(&addUserInSegmentsRequest); err != nil {
		apiError := model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest}
		util.SendError(context, &apiError)
		return
	}

	userInfoResponse, err := c.UserService.UpdateUserSegments(&addUserInSegmentsRequest)
	if err != nil {
		util.SendError(context, err)
		return
	}

	context.JSON(http.StatusOK, userInfoResponse)
}

// GetUserById godoc
//
//		@Summary		Метод получения активных сегментов пользователя. Принимает на вход id пользователя.
//		@Description	Метод получения активных сегментов пользователя. Принимает на вход id пользователя.
//		@Tags			User API
//		@Produce		json
//	 	@Param 			id   			path      int  		true  "User ID"
//		@Success		200	{object}	model.UserInfoResponse "DTO с информацией о пользователе и его сегментах"
//		@Failure        404 {object}    model.ApiError "User not found"
//		@Router			/api/v1/user/{id} [get]
func (c *UserControllerImpl) GetUserById(context *gin.Context) {
	userIdString := context.Param("id")

	userId, err := strconv.Atoi(userIdString)
	if err != nil {
		util.SendError(context, &model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest})
		return
	}
	userResponse, apiError := c.UserService.FindUserById(int64(userId))
	if apiError != nil {
		util.SendError(context, apiError)
		return
	}

	context.JSON(http.StatusOK, userResponse)
}

// AddUserInSegment godoc
//
//		@Summary		Метод добавлениея пользователя в сегмент на время
//		@Description	Метод добавлениея пользователя в сегмент на время
//		@Tags			User API
//		@Accept			json
//		@Produce		json
//	 	@Param 			example 		body model.AddUserInSegmentRequest true "Запрос на добавление пользователя в сегмент на время"
//		@Success		200	{object}	model.UserInfoResponse "DTO с информацией о пользователе и его сегментах"
//		@Failure        404 {object}    model.ApiError "User not found"
//		@Router			/api/v1/user/segment [post]
func (c *UserControllerImpl) AddUserInSegment(context *gin.Context) {
	var request model.AddUserInSegmentRequest

	if err := context.ShouldBindJSON(&request); err != nil {
		apiError := model.ApiError{Message: err.Error(), StatusCode: http.StatusBadRequest}
		util.SendError(context, &apiError)
		return
	}

	response, apiError := c.UserService.AddUserInSegmentWithExpirationDate(request)
	if apiError != nil {
		util.SendError(context, apiError)
		return
	}

	context.JSON(http.StatusOK, response)
}
