package entity

import "time"

// TransactionEntity Сущность транзакция
type TransactionEntity struct {

	// Иденнтификатор транзакции
	Id int64 `gorm:"primaryKey;not null;unique;integer;column:id"`

	// Идентификатор пользователя
	UserId int64 `gorm:"not null;integer;column:user_id"`

	// Идентификатор сегмента
	SegmentId int64 `gorm:"column:segment_id"`

	// Название сегмента
	Segment SegmentEntity

	// Название сегмента
	Type string `gorm:"not null;varchar;column:type"`

	// Время создания транзакции
	CreatedAt time.Time `gorm:"autoCreateTime:true"`
}

func (e *TransactionEntity) TableName() string {
	return "transactions"
}
