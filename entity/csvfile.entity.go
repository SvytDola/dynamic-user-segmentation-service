package entity

// CsvFileEntity Сущность "Csv File" хранит в себе информацию с транзакциями пользователя
type CsvFileEntity struct {

	// Идентификатор файла
	Id int64 `gorm:"primaryKey;not null;unique;integer;column:id"`

	// Содержание файла
	Text string `gorm:"not null;string;column:text"`
}
