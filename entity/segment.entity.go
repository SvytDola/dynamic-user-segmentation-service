package entity

// SegmentEntity Сущность "Сегмент" таблицы "segments"
type SegmentEntity struct {

	// Идентификатор сегмента
	Id int64 `gorm:"primaryKey;not null;unique;integer;column:id"`

	// Название сегмента
	Slug string `gorm:"size:255;not null;unique;varchar;column:slug" example:"AVITO_DISCOUNT_50"`

	// Пользователи сегмента
	Users []UserEntity `gorm:"many2many:user_segments"`

	// Транзакции сегмента
	Transactions []TransactionEntity `gorm:"foreignKey:SegmentId;references:Id"`
}

func (SegmentEntity) TableName() string {
	return "segments"
}

// Contains {true} - если сегмент есть в списке, {false} - если сегмента нет в списке.
func (e SegmentEntity) Contains(entities []SegmentEntity) bool {
	for _, segment := range entities {
		if segment.Id == e.Id {
			return true
		}
	}
	return false
}
