package entity

// UserEntity
// @Description Сущность "Пользователь" таблицы "users"
type UserEntity struct {

	// Идентификатор пользователя
	Id int64 `gorm:"primaryKey;not null;unique;integer;column:id"`

	// Сегменты, в которых состоит пользователь
	Segments []SegmentEntity `gorm:"many2many:user_segments"`

	// История сегментов пользователя
	Transactions []TransactionEntity `gorm:"foreignKey:UserId;references:Id"`
}

func (UserEntity) TableName() string {
	return "users"
}
