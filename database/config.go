package database

type ConfigDatabase struct {
	Port     string `env:"DB_PORT" env-default:"5432"`
	Host     string `env:"DB_HOST" env-default:"localhost"`
	Name     string `env:"DB_NAME" env-default:"db"`
	User     string `env:"DB_USER" env-default:"postgres"`
	Password string `env:"DB_PASSWORD" env-default:"postgres"`
	Swagger  string `env:"SWAGGER" env-default:"true"`
}

type TestConfigDatabase struct {
	Port     string `env:"TEST_DB_PORT" env-default:"5432"`
	Host     string `env:"TEST_DB_HOST" env-default:"localhost"`
	Name     string `env:"TEST_DB_NAME" env-default:"db-test"`
	User     string `env:"TEST_DB_USER" env-default:"postgres"`
	Password string `env:"TEST_DB_PASSWORD" env-default:"postgres"`
	Swagger  string `env:"SWAGGER" env-default:"false"`
}
