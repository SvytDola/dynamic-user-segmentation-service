package database

import (
	"dynamic-user-segmentation-service/entity"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
)

// ConnectToDatabase Подключение к базе данных.
func ConnectToDatabase(dialector gorm.Dialector) *gorm.DB {
	database, err := gorm.Open(dialector, &gorm.Config{Logger: logger.Default.LogMode(logger.Info)})

	if err != nil {
		panic(err)
	} else {
		fmt.Println("Successfully connected to the database")
	}
	return database
}

// AutoMigrate Автоматическое создание таблиц, если их не сущесвует.
func AutoMigrate(database *gorm.DB) {
	err := database.AutoMigrate(
		&entity.UserEntity{},
		&entity.SegmentEntity{},
		&entity.TransactionEntity{},
		&entity.CsvFileEntity{})
	if err != nil {
		log.Println(err)
	}
}
