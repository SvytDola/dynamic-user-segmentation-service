package mapper

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
)

type UserMapper struct {
}

// ToResponse Преобразует сущность entity.UserEntity в model.UserInfoResponse.
func (m *UserMapper) ToResponse(userEntity *entity.UserEntity) *model.UserInfoResponse {
	segmentResponses := make([]model.SegmentResponse, len(userEntity.Segments))

	for index, segment := range userEntity.Segments {
		segmentResponses[index] = model.SegmentResponse{Slug: segment.Slug, Id: segment.Id}
	}

	return &model.UserInfoResponse{Id: userEntity.Id, Segments: segmentResponses}
}
