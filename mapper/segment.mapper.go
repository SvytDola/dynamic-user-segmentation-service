package mapper

import (
	"dynamic-user-segmentation-service/entity"
	"dynamic-user-segmentation-service/model"
)

type SegmentMapper struct {
}

// ToResponse Преобразует entity.SegmentEntity в model.SegmentResponse.
func (m *SegmentMapper) ToResponse(entity *entity.SegmentEntity) *model.SegmentResponse {
	return &model.SegmentResponse{Id: entity.Id, Slug: entity.Slug}
}

// ToEntity Преобразует model.SegmentCreateRequest в entity.SegmentEntity.
func (m *SegmentMapper) ToEntity(segmentCreateRequest *model.SegmentCreateRequest) *entity.SegmentEntity {
	return &entity.SegmentEntity{Slug: segmentCreateRequest.Slug}
}
